#include <vector>
#include <set>
#include <iostream>
#include <string>
#include <queue>
#include <algorithm>
#include <iomanip>

using namespace std;

const int N = 109; //upper limit
int statesNum, symbolsNum; 
vector< vector< vector<int> > > epsilonStructure;
vector< vector< vector<int> > > nfa;
vector< vector< vector<int> > > intermediate;
set<int> closure[N];


int dfa[N][N]; // dfa table
vector<int> dfaStates[N]; // dfa states
int totalDfaStates = 0; // states number in dfa

//function to remove duplicates 
vector<int> removeDuplicates(vector<int> &v) {
  if(v.size() == 0) return v;
  sort(v.begin(), v.end());
  vector<int> result;
  
  result.push_back(v[0]);
  for(int i = 1; i < v.size(); i++) {
    if(v[i - 1] != v[i]) {
      result.push_back(v[i]);
    } else {
      while(v[i-1] == v[i]) i++;
      i--;
    }
  }
  return result;
}


void printNfa() {
    cout << "\nNFA :\n";
    cout << "============================\n";
    cout << "Q\t\tSymbols\n";

    for (int i = 0; i < statesNum; i++) {
        cout << "Q" << i << "\t";
        for (int j = 1; j <= symbolsNum; j++) {
            cout << "{";
            for (int ii : nfa[i][j]) cout << ii << " ";
            cout << "}\t";
        }
        cout << endl;
    }
    cout << endl;
}

void printDfa() {
		cout << "\n DFA Table:\n";
		cout << "================\n";
		cout << "Q";
		for(int j=0; j<symbolsNum; j++) {
			cout << setw(10) << j;
		}
		cout << endl;
		for(int i=0; i<totalDfaStates; i++) {
			cout << "[";
			for(int k=0; k < dfaStates[i].size(); k++) cout << dfaStates[i][k]; cout << "]";
      
			for(int j=0; j<symbolsNum; j++) {
				cout << setw(10) << "[";
				for(int k=0; k<dfaStates[dfa[i][j]].size(); k++) {
					cout << dfaStates[dfa[i][j]][k];
				}
				cout << "]";
			}
			cout << endl;
		}
  
	 cout << endl;
}

//check if the state is final
bool isFinal(int state, const vector<int> &final) {
  for(int i = 0; i < final.size(); i++) {
    if(final[i] == state)
      return true;
  }
  return false;
}


void addToBox(int row, vector< vector< vector<int> > > &epsilonStructure, queue<int> &box) {
  for(int i = 0; i < epsilonStructure[row][0].size(); i++) {
    box.push(epsilonStructure[row][0][i]);
  }
}

//to find if there is a path from initial state to final state with epsilon (epsilon nfa ---> nfa)
void checkFinalStates(vector<int> &finalStates, vector< vector< vector<int> > > &epsilonStructure) {
  
  queue<int> box;
  addToBox(0, epsilonStructure, box); 
  bool found = false;

  vector<int> visited(epsilonStructure.size(), 0);

  while(!box.empty()) {
      
      int temp = box.front();
      box.pop();
      if(visited[temp])continue;
      visited[temp] = 1;
      if(isFinal(temp, finalStates)) {
        found = true;
        break;
      }
      addToBox(temp, epsilonStructure, box);
  }

  if(found) {
    finalStates.push_back(0);
  }
}

void printStates(int states) {
  //print states 
  cout << "Q: {";
  cout << "0";
  for (int i = 1; i < statesNum; i++) {
    cout << ", " << i;
  }
  cout << "}\n";
}

void printInputSymbols(vector<string> &symbols) {
  //print input sympols
  cout << "Sigma: {";
  cout << symbols[0];
  for(int i = 1; i < symbols.size(); i++) {
    cout << ", " << symbols[i];
  }
  cout << "}\n";
}

//function to print the results (nfa) into file
void printNFAToFile(int states, vector<string> &symbols, vector<int> &finalStates) {
  freopen("NFA.txt", "w", stdout);
  
  printStates(states);
  printInputSymbols(symbols);

  //print delta
  cout << "Delta : \n";
  printNfa();
  
  //print initial state
  cout << "q0: 0\n";

  //print final states
  sort(finalStates.begin(), finalStates.end());
  cout << "Final States: {";
  cout << finalStates[0];
  for(int i = 1; i < finalStates.size(); i++) {
    cout << ", " << finalStates[i];
  }
  cout << "}";
}

//function to print the results (dfa) into file
void printDFAToFile(int states, vector<string> &symbols, vector<int> &finalStates) {
  freopen("DFA.txt", "w", stdout);

  printStates(states);
  cout << endl;

  printInputSymbols(symbols);
  cout << endl;
  
  //print delta
  cout << "Delta:";
  printDfa();


  //print initial state
  cout << "q0: 0\n\n";

  // Removing duplicates from the final state
  sort(finalStates.begin(), finalStates.end());
  finalStates.resize(unique(finalStates.begin(), finalStates.end()) - finalStates.begin());

  //print transition table
  cout << "Final States: {[";
  for(int k=0; k < dfaStates[finalStates[0]].size(); k++) cout << dfaStates[0][k]; cout << "]";
  for(int i=1; i<finalStates.size(); i++) {
    cout << ", [";
    for(int k=0; k < dfaStates[finalStates[i]].size(); k++) cout << dfaStates[finalStates[i]][k]; cout << "]";
  }
  cout << "}";

}

void createNFA() {

    // Finding epsilon path from all states
    for (int i = 0; i < statesNum; i++) {
        queue<int> q;
        vector<bool> visited(statesNum, false);
        q.push(i); visited[i] = 1;
        while (!q.empty()) {
            int temp = q.front(); q.pop();
            for (int k = 0; k < epsilonStructure[temp][0].size(); k++) {
                int current = epsilonStructure[temp][0][k];
                if (visited[current] == 0) {
                    visited[current] = 1;
                    q.push(current);
                }
            }
        }
        for (int j = 0; j < statesNum; j++) {
            if (visited[j] == 1) 
            closure[i].insert(j);
        }
    }

    nfa.resize(statesNum);
    for(int i = 0; i < statesNum; i++) {
      nfa[i].resize(symbolsNum+1);
    }

    for (int i = 0; i < statesNum; i++) {
      for (int i2 : closure[i]) {
        for (int j = 1; j <= symbolsNum; j++) {
          for (int k = 0; k < epsilonStructure[i2][j].size(); k++) {
            int current2 = epsilonStructure[i2][j][k];
            for (int i3 : closure[current2]) {
                nfa[i][j].push_back(i3);
            }
          }
        }
      }
    }
    
    for(int i = 0; i < statesNum; i++) {
      for (int j = 0; j <= symbolsNum; j++) {
        nfa[i][j] = removeDuplicates(nfa[i][j]);
      }
    }
    
}

//fill the data from the user
void enterData(vector<string> &symbols, vector<int> &finalStates) {
  cout << "Enter the 5 tuples M = (Q, Segma, Delta, q0, F) :\n";

  cout << "What is the number of states? ";
  cin >> statesNum;
  epsilonStructure.resize(statesNum);

  cout << "How many input symbols? ";
  cin >> symbolsNum;

  for(int i = 0; i < statesNum; i++) {
    epsilonStructure[i].resize(symbolsNum+1);
  }

  for(int i = 0; i < symbolsNum; i++) {
    cout << "Enter symbol #" << i+1 << ": ";
    string temp;
    cin >> temp;
    symbols.push_back(temp);
  }

  cout << "\nHow many final states? ";
    int f;
    cin >> f;
    for(int i = 0; i < f; i++) {
      cout << "Enter final state #" << i + 1 << ": ";
      int tempInt;
      cin >> tempInt;
      finalStates.push_back(tempInt);
    }

    cout << "\n\nNow follow along to fill the transition table\n_________________________________________________\n";

    for (int i = 0; i < statesNum; i++) {
      cout << "\nEnter Transitions for State #" << i << endl;
      for (int j = 0; j <= symbolsNum; j++) {
        cout << "\tHow many ";
        if (j == 0) cout << "epsilon";
        else cout << symbols[j - 1];
        cout << " transitions? ";
        int temp; cin >> temp;
        epsilonStructure[i][j].resize(temp);
        
        if (temp == 1) {
          cout << "\tWhat is the state? ";
        }
        else if (temp > 0 ) {
          cout << "\tEnter the " << temp << " states: ";
        }
        for (int k = 0; k < epsilonStructure[i][j].size(); k++) {
          cin >> epsilonStructure[i][j][k];
        }
      }
      cout << "===================================";
      cout << endl;
    }
}

void createDfa(vector<int> finalStates, vector<int> &dfaFinalStates) {

  // Create a queue for visiting all nodes "BFS style".
  queue<int> q;
  
  // Add the initial state 0
  vector<int> v; v.push_back(0); q.push(0);

  // Checking if 0 is a final state
  if(isFinal(0, finalStates)) {
    dfaFinalStates.push_back(0);
  }

  // Adding 0 as the first state
  dfaStates[totalDfaStates++] = v;

  // Visiting all states
   while(!q.empty()) {
    int top = q.front(); q.pop();
    for(int j=1; j<=symbolsNum; j++) {
      vector<int> current;
      
      // Adding the children of all states inside the current state
      for(int i=0; i < dfaStates[top].size(); i++) {
        for(int k=0; k < nfa[dfaStates[top][i]][j].size(); k++) {
          current.push_back(nfa[dfaStates[top][i]][j][k]);
        }
      }

      // Sorting and removing Duplicates
      sort(current.begin(), current.end());
      current.resize(unique(current.begin(), current.end()) - current.begin());

      // Finding the position of the state if it was seen before
      int position = -1;
      for(int i=0; i<totalDfaStates; i++) {
        if(dfaStates[i] == current) {
          position = i;
          break;
        }
      }
      if(position == -1) {
        for(int i = 0; i < current.size(); i++) {
          if(isFinal(current[i], finalStates)) 
            dfaFinalStates.push_back(totalDfaStates);
        }
        dfaStates[totalDfaStates] = current;
        q.push(totalDfaStates);
        dfa[top][j-1] = totalDfaStates;
        totalDfaStates++; 
      } else {
        dfa[top][j-1] = position;
      }
      
    }
   	}
 }

int main() {

  vector<string> symbols;
  vector<int> finalStates;

  //fill the data
  enterData(symbols, finalStates);

  //epsilon nfa ----> nfa phase
  createNFA();
  checkFinalStates(finalStates, epsilonStructure);
  printNFAToFile(statesNum, symbols, finalStates);
  
  //nfa ----> dfa phase
  vector<int> dfaFinalStates;
  createDfa(finalStates, dfaFinalStates);
  printDFAToFile(statesNum, symbols, dfaFinalStates);
  
  return 0;
}
